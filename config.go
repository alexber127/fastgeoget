package main

import (
	"encoding/json"
	"log"
	"os"
)

type ConfigStruct struct {
	Host      string `json:"host"`
	Port      string `json:"port"`
	DbHost    string `json:"dbHost"`
	DbName    string `json:"dbName"`
	YandexKey string `json:"yandexKey"`
	YandexUrl string `json:"yandexUrl"`
}

var Config ConfigStruct

func init() {
	file, _ := os.Open("config.json")
	decoder := json.NewDecoder(file)
	if err := decoder.Decode(&Config); err != nil {
		log.Fatalln(err.Error())
	}
}
