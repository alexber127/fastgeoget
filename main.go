package main

import (
	"crypto/md5"
	"encoding/hex"
	"log"
	"net/http"
	"time"

	"gopkg.in/mgo.v2/bson"
)

func main() {
	http.HandleFunc("/", indexAction)
	http.HandleFunc("/query", queryAction)
	log.Fatal(http.ListenAndServe(Config.Host+":"+Config.Port, nil))
}

func indexAction(w http.ResponseWriter, r *http.Request) {
	tpl := Tpls{}
	tpl.Parse("index").Render(w, map[string]string{"title": "Test yandex query"})
}

func queryAction(w http.ResponseWriter, r *http.Request) {
	resp := Response{}
	defer HandleClose(w)

	r.ParseForm()
	query := r.Form.Get("query")
	if query == "" {
		panic("query can not be empty")
	}

	hasher := md5.New()
	hasher.Write([]byte(query))
	hash := hex.EncodeToString(hasher.Sum(nil))

	if c := Count("logs_query", bson.M{"hash": hash}); c > 0 {
		var itemLog LogItem
		FindItem("logs_query", bson.M{"hash": hash}, &itemLog)
		resp.SetValue(itemLog.Data)
	} else {
		yandex := Yandex{}
		yandex.FormQuery(query).GetResult()
		go Insert("logs_query", LogItem{
			Query:   query,
			Hash:    hash, // Important: need index field
			Data:    yandex.Response,
			Created: time.Now(),
		})
		resp.SetValue(yandex.Response)
	}

	resp.Send(w)
}

func HandleClose(w http.ResponseWriter) {
	if msg := recover(); msg != nil {
		resp := Response{}
		resp.SetError(msg.(string))
		resp.Send(w)
	}
}
