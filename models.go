package main

import (
	"log"
	"time"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type LogItem struct {
	Id      bson.ObjectId          `json:"_id,omitempty" bson:"_id,omitempty"`
	Query   string                 `json:"query" bson:"query"`
	Hash    string                 `json:"hash" bson:"hash"`
	Created time.Time              `json:"created" bson:"created"`
	Data    map[string]interface{} `json:"data" bson:"data"`
}

var (
	sessMongo *mgo.Session
	dbHost    string
	dbName    string
)

const PagenationLimit = 20

func init() {
	dbHost = Config.DbHost
	dbName = Config.DbName

	var err error
	sessMongo, err = mgo.Dial(dbHost)
	if err != nil {
		log.Fatalln("Error connect MongoDB")
	}
	sessMongo.SetMode(mgo.Monotonic, true)
}

func Count(nameColl string, in interface{}) int {
	session := getConnect()
	defer session.Close()

	if num, err := session.DB(dbName).C(nameColl).Find(in).Count(); err == nil {
		return num
	}
	return 0
}

func Find(nameColl string, in interface{}, out interface{}, sortable ...interface{}) error {
	session := getConnect()
	defer session.Close()

	sortItem := "-created"

	if len(sortable) > 0 {
		sortItem = sortable[0].(string)
	}

	return session.DB(dbName).C(nameColl).Find(in).Sort(sortItem).All(out)
}

func FindLimit(nameColl string, in interface{}, out interface{}, sortable string, limit int) error {
	session := getConnect()
	defer session.Close()
	return session.DB(dbName).C(nameColl).Find(in).Sort(sortable).Limit(limit).All(out)
}

func FindItem(nameColl string, in interface{}, out interface{}, sortable ...interface{}) error {
	session := getConnect()
	defer session.Close()

	sortItem := "-created"

	if len(sortable) > 0 {
		sortItem = sortable[0].(string)
	}

	return session.DB(dbName).C(nameColl).Find(in).Sort(sortItem).One(out)
}

func FindById(nameColl string, in interface{}, out interface{}) error {
	session := getConnect()
	defer session.Close()

	return session.DB(dbName).C(nameColl).FindId(in).One(out)
}

func getConnect() *mgo.Session {
	if sessMongo == nil {
		sessMongo.Refresh()
		return sessMongo.Copy()
	}
	return sessMongo.Copy()
}

func Insert(nameColl string, in interface{}) (error, bson.ObjectId) {
	session := getConnect()
	defer session.Close()

	if nameColl == "users" {
		err := session.DB(dbName).C(nameColl).Insert(in)
		return err, bson.NewObjectId()
	}

	idObject := bson.NewObjectId()
	_, err := session.DB(dbName).C(nameColl).UpsertId(idObject, in)
	return err, idObject
}

func Update(nameColl string, query interface{}, in interface{}) error {
	session := getConnect()
	defer session.Close()
	return session.DB(dbName).C(nameColl).Update(query, in)
}

func UpdateById(nameColl string, id interface{}, in interface{}) error {
	session := getConnect()
	defer session.Close()
	return session.DB(dbName).C(nameColl).UpdateId(id, in)
}

func UpdateSetById(nameColl string, id interface{}, in interface{}) error {
	session := getConnect()
	defer session.Close()
	update := bson.M{"$set": in}
	return session.DB(dbName).C(nameColl).UpdateId(id, update)
}

func UpdateMany(nameColl string, query interface{}, in interface{}) error {
	session := getConnect()
	defer session.Close()
	update := bson.M{"$set": in}
	_, err := session.DB(dbName).C(nameColl).UpdateAll(query, update)
	return err
}

func Delete(nameColl string, in interface{}) error {
	session := getConnect()
	defer session.Close()
	return session.DB(dbName).C(nameColl).Remove(in)
}

func DeleteAll(nameColl string, in interface{}) error {
	session := getConnect()
	defer session.Close()
	_, err := session.DB(dbName).C(nameColl).RemoveAll(in)
	return err
}

func DeleteById(nameColl string, in interface{}) error {
	session := getConnect()
	defer session.Close()
	return session.DB(dbName).C(nameColl).RemoveId(in)
}

func GetSess() *mgo.Session {
	return getConnect()
}

func GetColl(nameColl string) (*mgo.Session, *mgo.Collection) {
	session := getConnect()
	return session, session.DB(dbName).C(nameColl)
}

func GetLastDataWithLimit(nameCol string, result interface{}, limit int) error {
	sess, coll := GetColl(nameCol)
	defer sess.Close()

	sortItem := "-created"

	return coll.Find(nil).Sort(sortItem).Skip(0).Limit(limit).All(result)
}

func GetLastDataWithLimitAndQuery(nameCol string, result interface{}, limit int, query bson.M) error {
	sess, coll := GetColl(nameCol)
	defer sess.Close()

	sortItem := "-created"

	return coll.Find(query).Sort(sortItem).Skip(0).Limit(limit).All(result)
}

func CheckObjectId(str string) bool {
	if str == "" {
		return false
	}

	chk := func() bool {
		if r := recover(); r != nil {
			return false
		}
		return true
	}

	defer chk()

	obj := bson.ObjectIdHex(str)
	return obj.Valid()
}
