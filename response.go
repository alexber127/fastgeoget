package main

import (
	"encoding/json"
	"log"
	"net/http"
)

type Response struct {
	Status bool          `json:"status"`
	Errors []string      `json:"errors"`
	Values []interface{} `json:"values"`
}

func (r *Response) SetError(errorItem string) {
	r.Errors = append(r.Errors, errorItem)
}

func (r *Response) SetValue(item interface{}) {
	r.Values = append(r.Values, item)
}

func (r *Response) Send(w http.ResponseWriter) {
	if len(r.Errors) == 0 {
		r.Status = true
	}
	w.Header().Add("Content-Type", "application/json")
	bytes, err := json.Marshal(r)
	if err != nil {
		log.Fatalln(err.Error())
	}
	w.Write(bytes)
}
