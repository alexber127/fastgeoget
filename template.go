package main

import (
	"html/template"
	"net/http"
)

type Tpls struct {
	tpl *template.Template
}

var tplFolder string = "tpls"

func (t *Tpls) Parse(name string) *Tpls {
	blocksFiles := []string{}
	blocksFiles = append(blocksFiles, tplFolder+"/"+name+".html")
	tmpl, err := template.ParseFiles(blocksFiles...)
	if err != nil {
		panic(err.Error())
	}
	t.tpl = tmpl
	return t
}

func (t *Tpls) Render(w http.ResponseWriter, data interface{}) {
	if err := t.tpl.Execute(w, data); err != nil {
		panic(err.Error())
	}
}
