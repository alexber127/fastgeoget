package main

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/url"
)

type Yandex struct {
	Url      string
	Response map[string]interface{}
}

func (y *Yandex) FormQuery(query string) *Yandex {
	urlSegments := url.Values{}
	urlSegments.Add("apikey", Config.YandexKey)
	urlSegments.Add("geocode", query)
	urlSegments.Add("lang", "en-US")
	urlSegments.Add("format", "json")
	y.Url = Config.YandexUrl + urlSegments.Encode()
	return y
}

func (y *Yandex) GetResult() {
	response, err := http.Get(y.Url)
	if err != nil {
		panic(err.Error())
	}
	defer response.Body.Close()
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		panic(err.Error())
	}
	if err := json.Unmarshal(body, &y.Response); err != nil {
		panic(err.Error())
	}
}
